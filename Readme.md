pyBumper
===========
pyBumper is an email reminder service a la nudgemail or followupthen that you can run using your personal domain and server. I'm using it for:

* Pushing long-term tasks cannot be done now but deserve more consideration further down the road. You may argue that many of them should just not be done. In my experience, this kind of treatment actually helps with this goal. When they come back up, tasks often seem much less significant and it becomes much easier to abandon them.
* Reminding yourself about responses that you're expecting from, e.g., co-workers. For instance, when you send out a question email, you can also add a BCC version to your reminder address that will be sent back to you at the specified time. At that point, you can then decide whether you should follow-up again.

Why should you use my application instead of the aforementioned services?

* Your data stays on your server. Only you can decide how important that is to you.
* You build robustness into your life. Granted, this is a one man show right now, but short of major Python changes, I don't see why this should stop working. Email and IMAP has been around for a while after all. In addition, I have a personal interest in this and will probably fix upcoming problems and even if I don't you can take over the source code and make the necessary adjustments by yourself. The companies on the other hand don't owe you anything and could change their terms at any time.

Date Examples
------
* 7d.bp@domain.com : Returns after 7 days. (constantly used by me, so best tested)
* 2014-12-24.bp@domain.com : Returns to you on Christmas (constantly used by me, so best tested)
* Sep.bp@domain.com or Oct2016.bp@domain.com : Returns on first of next September / Oct 2016 (less used)
* Tue.bp@domain.com : Emails back next Tuesday (less used)
* Others, i.e., those that the parser didn't understand are returned today.

WARNING
-------
* Not all webhosts will let you do the kind of setup that is required to run this!
* We are not storing attachments that are sent along with email; only the text inside them!


Pre-requisites
======
Mail requirements (Domain, IMAP)
-----------
The script polls an email server. To make use of the date information you need to have all emails to this wide variety of addresses go to a single IMAP folder there. There are probably very smart ways to do this if you have full control over your email server. I achieved this in the following way with my standard webhost package.

* Create a catchall email address that takes all email that is not taken care of by explicitly defined addresses. On my webhost this was achieved by directing ``*@domain.com`` to an email box that I created.
* Create a filter in the corresponding email account as follows: received matches regular expression "[\_.]bp@domain.com" with actions: move to folder (e.g. bumper_infolder), mark as read, stop checking. 
* I also have a filter that is at the end of the processing and moves all remaining email to a spam folder. I sort away the email I want to keep before that. You'll receive all kind of random stuff on a catchall address otherwise.This isn't relevant for this script however.


Server requirements (Python environment)
-----------
Before you begin, make sure you have the following Python environment set up. I have tested this on my Windows machine (cronjobs won't work there though, so you need to figure out the scheduler on your own), my newer Ubuntu home server and the old Ubuntu version that is running on my vserver. You should probably get this running on a Raspberry Pi as well.

* python-dateutil (I needed easy_install for this on my vserver, pip and apt-get wouldn't work; they should work on most more current Linux flavors however)
* Python 2.7 (what I tested with, wouldn't exclude that it works with other versions however)
* Others: Please let me know about requirements that I forgot!

Installation
======
I'm a single user at this point, so please contact me if you're going through this and it doesn't work immediately!

Download
-----------
Pull latest version from bitbucket using mercurial or download and extract to folder of your choice using the download link.

Main Configuration
-------------
Copy Config.default to Config.py and adjust it to your needs. User name and password should be stored usign netrc. This is slightly more secure although far from perfect. At least, it prevents you from checking your password into repositories.

Login data via netrc
-----------
on Windows, create file _netrc in same folder;
otherwise create file ~/.netrc
also see: http://www.mavetju.org/unix/netrc.php

File format:

    machine mail.<server_address> //mail only helps me to find this entry!
    login <xxx>
    password <yyy>	

Cron Job
==========
You can run this script as often as you want. The idea is to run it once a day around midnight such that tasks appear in your mailbox in the morning. Remember, this isn't meant for appointments that require precision rather for tasks that you push out longer into the future. Open via 'crontab -e' with the user that you want to run the job. My crontab looks as follows (I believe a new line is mandatory at the end; Bumper.py changes the working directory, cron starting in your home directory should therefore not lead to confusion):

    2 */3 * * * python /home/matthias/pybumper/Bumper.py &>> /home/matthias/pybumper/cron_error.log

This executes the script every 3 hours at 2 minutes after the hour. The error output is redirected to cron_error.log because I wouldn't find it in the depths of the Linux space otherwise.

Contribution
===============
Consider the tasks I have put into todo.txt. Most importantly though, the project is in a somewhat rought state and most improvements are welcome :)
