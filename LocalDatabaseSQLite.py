"""Copyright (C) 2014License: This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at your
 option) any later version. This program is distributed in the hope that it
 will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.

"""

# -*- coding: utf-8 -*-
import sqlite3
import logging
import datetime
import EmailMessage
import Config
import unicodedata
# import email
# import email.mime.application

from LocalDatabaseInterface import LocalDatabaseInterface

class LocalDatabaseSQLite(LocalDatabaseInterface):
    """ Implements LocalDatabaseInterface with local sqlite data base in the background
    """
# class LocalDatabaseSQLite(object):
    
    def __init__(self, cfg):
        self.cfg = cfg
        self.fname = "bumper.sqlite"
        self.db = sqlite3.connect(self.fname)
        c = self.db.cursor()
#         self.msg_keys = ['out_date', 'to_addr', 'from_addr', 'subject', 'content', 'email_date', 'date_addr']
        
        create_str = """ CREATE TABLE IF NOT EXISTS bumper (
        content TEXT, email_date TEXT, date_addr TEXT, from_addr TEXT, 
        out_date INT, subject TEXT, to_addr TEXT
        )"""
        c.execute(create_str)
        
    def __del__(self):
        self.db.close()
        
    def append(self, msg_list):
        """ Append list of messages to data base
        
        @param msg_list List of email messages (as in EmailMessage.py)
        """
        logging.info("Appending to sqlite DB")
        logging.debug(msg_list)
        c = self.db.cursor()
        istr2 = "INSERT INTO bumper(" + ", ".join(EmailMessage.msg_keys) + ")\n"
        istr2 += "VALUES(:" + ", :".join(EmailMessage.msg_keys) + ")\n"

        msg_list_with_ts = []
        for msg in msg_list:
            msgd = msg._asdict()
            timestamp = int( (msg.out_date - datetime.date(1970, 1, 1)).total_seconds() )
            msgd["out_date"] = timestamp
            for key in [k for k in EmailMessage.msg_keys if k not in ["out_date"]]:
                logging.debug(msgd[key])
                if not isinstance(msgd[key], unicode):
                    msgd[key] = unicode(msgd[key], "utf-8")

            msg_list_with_ts.append(msgd)

        c.executemany(istr2, msg_list_with_ts)
        self.db.commit()
        
    @staticmethod
    def db_rows_to_dict(all_rows):
        """ Converts all rows that are returned by sqlite connector to a list of EmailMessage
        
        @param all_rows data base rows as returned by cursor.fetchall() 
        @return: List of EmailMessage
        """
        msg_list = []
        for row in all_rows:
            i = 0
            msg_dict = {}
            for key in EmailMessage.msg_keys:
                if key in ["out_date"]:
                    msg_dict[key] = datetime.date.fromtimestamp(row[i])
                else:
                    msg_dict[key] = row[i]
                i+=1
            msg_list.append(EmailMessage.EmailMessage._fromdict(msg_dict))
        return msg_list
    
    @staticmethod
    def msg_list_to_str(msg_list):
        s = ""
        for msg in msg_list:
            s += str(msg) + "\n\n"
            
        return s
 
    def get_all(self):
        """ Returns all rows in data base; this can't rely on find newer b/c it would retrigger retrieve
        @return: List of EmailMessage
        """
        logging.info("SQLite: Finding all messages in database")       

        select_str = "SELECT " + ", ".join(EmailMessage.msg_keys) + " FROM bumper" + \
        " ORDER BY out_date ASC"
        logging.debug(select_str)
        
        c = self.db.cursor()
        c.execute(select_str)
        all_rows = c.fetchall()       
        
        msg_list = LocalDatabaseSQLite.db_rows_to_dict(all_rows)
        return msg_list

    def __get_compare_timestamp(self, compare_date):
        comp_ts = int( (compare_date - datetime.date(1970, 1, 1)).total_seconds() )
        return comp_ts

    def get_newer(self, compare_date):
        """ Returns all rows in data base with out_date newer than compare_date,
        if to_addr contains retrieve, all emails are extracted and appended as result if the sender is authorized.
        @param compare_date: if out_date older than this datetime.date, it will be included in result
        @param clear_them: (bool) should emails be cleared from DB after return; False mostly for testing, will send over and over!
        @return: List of EmailMessage
        """
        logging.info("SQLite: Finding newer messages in database")       
        c = self.db.cursor()

        comp_ts = self.__get_compare_timestamp(compare_date)
        select_str = "SELECT " + ", ".join(EmailMessage.msg_keys) + \
        " FROM bumper where out_date<=" + str(comp_ts) + \
        " ORDER BY out_date ASC"
        logging.debug(select_str)
        
        c.execute(select_str)
        all_rows = c.fetchall()
        logging.debug(all_rows)
        msg_list = LocalDatabaseSQLite.db_rows_to_dict(all_rows)
           
        for k, msg in enumerate(msg_list): #namedtuples are immutable afaik
            if('retrieve' in msg.to_addr):
                msg_str = "Sorry, your email address is not in admin addresses and cannot retrieve."
                if any( [addr in msg.from_addr for addr in self.cfg.admin_addresses]):
#                 for auth_addr in self.cfg.admin_addresses:
#                     if auth_addr in msg.from_addr:
                    #had to select the more complicated reverse thing b/c addresses contain
                    #names potentially
                    fulldb = self.get_all()
                    msg_str = LocalDatabaseSQLite.msg_list_to_str(fulldb)

                msg= msg._replace(content=msg_str)
                
            msg = msg._replace(to_addr=msg.from_addr, from_addr=self.cfg.from_addr)
            msg_list[k] = msg

#         msg_list.append(EmailMessage.EmailMessage._fromdict(msg_dict))
        return msg_list

    def clear_newer(self, compare_date):
        c = self.db.cursor()
        comp_ts = self.__get_compare_timestamp(compare_date)
        
        del_str= "DELETE FROM bumper where out_date<=" + str(comp_ts)
        logging.debug(del_str)
        c.execute(del_str)           
        self.db.commit()
            
        
if __name__ == '__main__':
    cfg = Config.Config()
    L = LocalDatabaseSQLite(cfg)
    
    msg_list = []
    msg_list.append(EmailMessage.EmailMessage._fromdict({"email_date": "Wed, 19 Nov 2014 20:31:33 -0500", "out_date": datetime.date.today(),
                 "date_addr": "2014-11-19.bp@zuez.org",
                  "from_addr": "matthias@zuez.org", "to_addr" : "software@matthiaskauer.com",
                   "subject": "Halloooooo", "content": "Will nur mal hallo sagen"}))
    msg_list.append(EmailMessage.EmailMessage(out_date=datetime.date(2014, 11, 22), to_addr=u'retrieve.bp@zuez.org',
                        from_addr=u'Matthias Kauer <m-kauer@posteo.de>',
                         subject=u'retrieve test', content=u'', 
                         email_date=u'Sun, 23 Nov 2014 16:19:19 -0500',
                          date_addr=u'retrieve.bp@zuez.org'))
    L.append(msg_list)
    
#     print isinstance(u"asdf", str)
#     print unicode(u"retrieve.bp@zuez.org", "utf-8")
    print L.get_newer(datetime.date.today(), clear_them=False)
