"""Copyright (C) 2014License: This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at your
 option) any later version. This program is distributed in the hope that it
 will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.

"""
import re
import datetime
from dateutil import parser, relativedelta

def parse_address(addr_str, bp_suffix, email_date=datetime.date.today() ):
    """ This parses an email address string and extracts the date that is embedded within. Parses for day offset (yd.bp@domain.com), then precise date (2014-12-24.bp@domain.com), month (Dec2014.bp@domain.com), weekday (Wed.bp@domain.com) and finally month alone (Sep.bp@domain.com)

    @param bp_suffix : string that specifies part of email address we consider fixed, e.g. ".bp@domain.com"    
    @param email_date: For offets of x days, the offset is calculated using the date of the email (which you can insert here), else today is fine
    @return: datetime.date for the out_date that was embedded in the address
    """
    today = datetime.date.today()

    #number of days, e.g. 30d
    # match = re.search('<(.+' + self.cfg.bumper_parse_suffix + ')>',re_str,re.I)
    match = re.search('(\d+)d' + bp_suffix , addr_str, re.I)
    if match:
        delta = int( match.group(1))
        if(isinstance(email_date, datetime.date)):
            indate = email_date
        else:
            indate = parser.parse(email_date, fuzzy=True).date()
        return indate + datetime.timedelta(days=delta)
    
    out_date = today
    #precise date (e.g. 2012-10-28)
#     match = re.search('([\d-]{8,10})[\._]bp@zuez.org', addr_str, re.I)
    match = re.search('([\d-]{8,10})' + bp_suffix, addr_str, re.I)
    if match:
        try:
            out_date = parser.parse(match.group(1), dayfirst=True, 
                                    yearfirst=True).date()
        finally:
            return out_date
    
    #certain month, e.g. Dec2013 => send on the first
    match = re.search('(\w{3}\d{4})' + bp_suffix, addr_str, re.I)
    if match:
        try:
            out_date = parser.parse('01 ' + match.group(1)).date()
        finally:
            return out_date
    
    #weekday (3 letters), e.g. Mon, Tue - this will also capture months (Jan, Feb, etc.)
    match = re.search('(\w{3})'+ bp_suffix, addr_str, re.I)
    if match:
        day_or_month_str = match.group(1)
        days_of_week = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
        if day_or_month_str.lower() in days_of_week:
            try:
                out_date = parser.parse(day_or_month_str).date()
            finally:
                return out_date
        else: #assume a month is being indicated
            try:
                out_date = parser.parse('01 ' + day_or_month_str).date()
                if out_date < today:
                    out_date += relativedelta.relativedelta(years=1)
            finally:
                return out_date

    return today
        

if __name__ == '__main__':
    bp_suffix = '[\._]bp@zuez.org'
    NOW_DAY = datetime.datetime.now().replace(microsecond=0,hour=0, minute=0, second=0)
    test_cases = [
                  ('Jan.bp@zuez.org', datetime.date(2015,01,01)),
                  ('Sep.bp@zuez.org', datetime.date(2015,9,01)),
                  ('Jun.bp@zuez.org', datetime.date(2015,06,01)),
                  ('Wed.bp@zuez.org', parser.parse('Wednesday').date()),
                  ('tue.bp@zuez.org', parser.parse('Tuesday').date()),
                  ('Dec2013.bp@zuez.org', datetime.date(2013,12,01)),
                  ('Jan2015.bp@zuez.org', datetime.date(2015,01,01)),
                  ('Jul2014.bp@zuez.org', datetime.date(2014,07,01)),
                  ('2013-12-31_bp@zuez.org', datetime.date(2013,12,31)),
                  ('20131201.bp@zuez.org', datetime.date(2013,12,01)),
                  ('2015-01-07_bp@zuez.org', datetime.date(2015,01,07)),
                  ('19850728.bp@zuez.org', datetime.date(1985,07,28)),
                  ('30d.bp@zuez.org', datetime.date.today() + datetime.timedelta(days=30)),
                  ('126d.bp@zuez.org', datetime.date.today() + datetime.timedelta(days=126)),
                  ('577d.bp@zuez.org', datetime.date.today() + datetime.timedelta(days=577)),
                  ]
    
    for t in test_cases:
        date = parse_address(t[0], bp_suffix)
        print str(date) + ' vs ' + str(t[1])
        #date2=parser.parse(t[1], default=datetime.datetime.now()).replace(microsecond=0)
        #date2=parser.parse(t[1])
#         date2=parser.parse(t[1], default=datetime.datetime.now()).replace(microsecond=0).date()
        date2 = t[1] 
        if not date == date2:
            print "WARNING WARNING: " + str(date) + " vs " + str(date2)

    #more offset test cases
    print parse_address("30d.bp@zuez.org", bp_suffix, email_date="u'Wed, 19 Nov 2014 20:31:33 -0500'")
    print parse_address("35d.bp@zuez.org", bp_suffix, email_date="u'Wed, 19 Nov 2014 20:31:33 -0500'")
    print parse_address("39d.bp@zuez.org", bp_suffix, email_date="u'Wed, 19 Nov 2014 20:31:33 -0500'")
    print parse_address("40d.bp@zuez.org", bp_suffix, email_date="u'Wed, 19 Nov 2014 20:31:33 -0500'")
    print parse_address("36d.bp@zuez.org", bp_suffix )
    print parse_address("1d.bp@zuez.org", bp_suffix)
