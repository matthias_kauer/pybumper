"""Copyright (C) 2014License: This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at your
 option) any later version. This program is distributed in the hope that it
 will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.

"""

#http://dbader.org/blog/abstract-base-classes-in-python
from abc import ABCMeta, abstractmethod

class LocalDatabaseInterface(object):
#     http://stackoverflow.com/questions/7064925/python-metaclass-and-modgrammar
#only Python 3 has class .. (metaclass=ABCMeta) syntax
    __metaclass__=ABCMeta

    @abstractmethod
    def append(self, msg_dict):
        pass
        
    @abstractmethod
    def get_newer(self, compare_date, clear_them):
        pass
