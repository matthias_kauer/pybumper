"""Copyright (C) 2014License: This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at your
 option) any later version. This program is distributed in the hope that it
 will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.

"""
from Mailstore import Mailstore
from LocalDatabaseInterface import LocalDatabaseInterface
#from LocalDatabaseExcel import LocalDatabaseExcel
from LocalDatabaseSQLite import LocalDatabaseSQLite
import datetime
import os
import sys
import Config
import logging
        
class Bumper:
    """ Main Bumper class; this brings together local data base and mail server connection and calls them in the right order.
    """
    def __init__(self,cfg, localDB):
        """ Constructor
        
        @param cfg (Config structure as defined in Config.py)
        @param localDB A local data base that implements to LocalDataBaseInterface (currently only SQLite)
        """
        self.cfg = cfg
        self.M = Mailstore(cfg)
        if(isinstance(localDB, LocalDatabaseInterface)):
            self.L = localDB
        else:
            raise Exception("Your localDB does not conform to the DB interface")

    def get_all(self):
        """ Retrieves all emails from mail server (in specified folder), appends them to data base and deletes them from server.
        """
        msg_dict = self.M.retrieve_all()
        logging.debug("Retrieved: ")
        logging.debug(msg_dict)
        self.L.append(msg_dict)
        self.M.clear_all()
        
    def send_all(self):
        """ Obtains all messages that are due today. Clears them from DB and sends them out using the mail store.
        """
        cdate = datetime.date.today()
        send_list = self.L.get_newer(compare_date=cdate)
        self.M.send(send_list, from_addr=self.cfg.from_addr)
        self.L.clear_newer(compare_date=cdate)

if __name__ == '__main__':
    cfg = Config.Config()

    if cfg.working_dir and os.path.isfile(cfg.working_dir):
        os.chdir(cfg.working_dir)
    else:
        abspath = os.path.abspath(__file__)
        dname = os.path.dirname(abspath)
        os.chdir(dname)

    logging.basicConfig(filename='bumper.log', level=logging.DEBUG)
    logging.info("")
    logging.info("")
    logging.info("="*20)
    logging.info("Bumper execution started at " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    logging.info("="*20)
    
    L = LocalDatabaseSQLite(cfg)
    B = Bumper(cfg, L)
    B.get_all()
    B.send_all()
    
    logging.info("Script execution complete")
