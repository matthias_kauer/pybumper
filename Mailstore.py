"""Copyright (C) 2014License: This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at your
 option) any later version. This program is distributed in the hope that it
 will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.

"""

import imaplib
import datetime
import email
import logging
from smtplib import SMTP_SSL
import re
from parse_address import *
import EmailMessage

class Mailstore(object):
    '''
    Establishes SSL connection to IMAP server and sends emails over SMTP.
    '''

    def __init__(self, cfg):
        '''
        Constructor
        @param cfg: Config structure as in Config.py
        '''
        self.cfg = cfg
        self.M = imaplib.IMAP4_SSL(cfg.server, cfg.port)
        self.M.login(cfg.user, cfg.passwd)
        
        self.smtp = SMTP_SSL()
        self.smtp.connect(cfg.server, 465)
        self.smtp.login(cfg.user, cfg.passwd)
                
    def __exit__(self):
        self.M.close()
        self.M.logout()
        
    def search_bpfolder(self):
        """ Selects the bumper folder; this is required before searching and deleting
        @return typ (unclear what this is to me)
        @return data (again unclear, but needed to fetch emails in retrieve_all)
        """
        self.M.select(self.cfg.bumperfolder)
        try:
            typ, data = self.M.search(None, 'ALL')
        except:
            #http://stackoverflow.com/questions/3267234/i-cannot-search-sent-emails-in-gmail-with-python
            exc_str = "These are all available folders on your mailbox:\n"
            exc_str += self.M.list()
            exc_str += "\nYou selected: " + self.cfg.bumperfolder + " and it wasn't available.\n"
            raise Exception(exc_str)

        return typ,data[0]

    def retrieve_all(self):
        """ Retrieves all emails in bumper folder.
        
        @return: List of EmailMessage
        """
        logging.info("Retrieving emails from server")
        typ,data = self.search_bpfolder()    

        msg_list = []
        for num in data.split():
            typ, msg_data = self.M.fetch(num, '(RFC822)')
            for resp_part in msg_data:
                if isinstance(resp_part, tuple):
                    
                    msg = email.message_from_string(resp_part[1])

                    if any( [address in msg["From"] for address in self.cfg.user_addresses]):
                        msg_dict = {}
                        msg_dict["email_date"] = msg["Date"]
                        if msg.is_multipart():
                            p1 = msg.get_payload()
                            msg_dict['content'] = p1[0].get_payload()
                        else:
                            msg_dict['content']= msg.get_payload()
                            #for content
                        #http://stackoverflow.com/questions/17874360/python-how-to-parse-the-body-from-a-raw-email-given-that-raw-email-does-not
    
                        #must do these keys by hand b/c I changed internal keys now 
                        msg_dict["to_addr"] = msg["To"]
                        msg_dict["from_addr"] = msg["From"]
                        msg_dict["subject"] = msg["Subject"]
    
                        msg_dict['out_date'] = datetime.date.today()
                        msg_dict["date_addr"] = ""
                        
                        re_str = resp_part[1]
    #                     match = re.search('<(.+[\._]bp@zuez.org)>',re_str,re.I)
                        match = re.search('<(.+' + self.cfg.bumper_parse_suffix + ')>',re_str,re.I)
                        if match:
                            match_str = match.group(1)
                            msg_dict["date_addr"] = match_str
    
                            if not "retrieve" in match_str:
                                msg_dict['out_date']  = parse_address(match_str, self.cfg.bumper_parse_suffix, msg_dict['email_date'] ) #this returns today if unsuccessful
        
                        msg_list.append(EmailMessage.EmailMessage._fromdict(msg_dict))
    
        return msg_list

    def clear_all(self):
        """ Deletes entire bumper folder
        """
        logging.info("Clearing emails from IMAP server now")
        typ, msg_ids = self.search_bpfolder()
        
            # What are the current flags?
        for id in msg_ids.split():
            typ, response = self.M.fetch(id, '(FLAGS)')
            logging.debug('Flags before: ' + str(response))
                
            # Change the Deleted flag
            typ, response = self.M.store(id, '+FLAGS', r'(\Deleted)')
        
            # What are the flags now?
            typ, response = self.M.fetch(id, '(FLAGS)')
            logging.debug('Flags after: ' + str(response))
        
        # Really delete the message.
        typ, response = self.M.expunge()
        logging.debug('Expunged: ' + str(response))
            
    def send(self, msg_list, from_addr):
        """ Sends all emails 
        
        @param msg_list List of EmailMessage
        @param from_addr string that specifies your sender address
        """
        logging.info("Sending emails now")
        for msg in msg_list:
            logging.debug(msg)
#             emailMsg = self.convert_msg_to_email(from_addr, msg)
            emailMsg = msg._asemail()
            self.smtp.sendmail(emailMsg['From'], emailMsg['To'], emailMsg.as_string())
        logging.info("All emails sent")
        
