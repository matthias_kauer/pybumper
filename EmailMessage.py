"""Copyright (C) 2014License: This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at your
 option) any later version. This program is distributed in the hope that it
 will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.

"""

from collections import namedtuple
import email

msg_keys = ['out_date', 'to_addr', 'from_addr', 'subject', 'content',
             'email_date', 'date_addr']
class EmailMessage(namedtuple('EmailMessage', msg_keys) ):
    """ Email Message specifies a consistent format for email messages internally. Python email library (see _asemail) would've been the more direct choice, but it always looked different depending on what was sent.
    """
    @staticmethod
    def _fromdict(indict):
        """ Convert a dict (that has all required fields) to a namedtuple
        """
        #Note: _fields can be iterated http://stackoverflow.com/questions/6570075/getting-name-of-value-from-namedtuple
        #Gut feeling: This will not work for constructor however
        ret = EmailMessage(from_addr=indict['from_addr'], out_date=indict['out_date'],
                           to_addr=indict['to_addr'], subject=indict['subject'],
                           content=indict['content'], email_date=indict['email_date'],
                           date_addr = indict['date_addr'])
        return ret
    
    def _asemail(self):
        """ return a Python email structure with the data from the namedtuple filled in.
        """
        emailMsg = email.MIMEMultipart.MIMEMultipart('mixed')
        emailMsg['To'] = self.to_addr
        emailMsg['From'] = self.from_addr
        emailMsg['Subject'] = self.subject
        emailMsg.attach(email.mime.Text.MIMEText(self.content, _charset='utf-8'))
        
        return emailMsg
        
        
if __name__ == '__main__':
    msg_list = [{"email_date": "Wed, 19 Nov 2014 20:31:33 -0500", "out_date": "2014-11-30",
             "date_addr": "2014-11-19.bp@zuez.org",
              "from_addr": "matthias@zuez.org", "to_addr" : "software@matthiaskauer.com",
               "subject": "Halloooooo", "content": "Will nur mal hallo sagen"}]
    md = msg_list[0]
    e = EmailMessage._fromdict(md)
    
    print e
    print e._asdict()
    md2 = e._asdict()
    print md2['from_addr']
    print e.from_addr
    
    print e._asemail()
